import firestore from '@react-native-firebase/firestore';
import React, { useContext, useState } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import { Spacing } from '../components';
import { DigitInput } from '../components/6-digit';
import { Button } from '../components/core';
import { Text } from '../components/core/text';
import { Input } from '../components/input';
import { Box } from '../components/layout/box';
import { AuthContext } from '../provider/auth';
const styles = StyleSheet.create({
  determineteBox: {
    height: 4,
    width: 187,
    borderRadius: 4,
  },
});
export const Signup = () => {
  const { sendCode, verifyCode } = useContext(AuthContext);
  const [code, setcode] = useState();
  const [type, settype] = useState(1);
  const [cred, setcred] = useState({
    uid: null,
    number: null,
    firsname: null,
    lastname: null,
  });
  const section1 = () => {
    settype(type + 1);
    sendCode(cred.number);
  };
  const section2 = () => {
    verifyCode(
      `${code.pin1}${code.pin2}${code.pin3}${code.pin4}${code.pin5}${code.pin6}`,
      setcred,
      cred
    );
    settype(type + 1);
  };
  const section3 = () => {
    if (firestore) {
      firestore().collection('users').doc(cred.uid).set(cred);
    }
  };
  const Stepstep = () => {
    switch (type) {
      case 1:
        section1();
        break;
      case 2:
        section2();
        break;
      case 3:
        section3();
        break;
    }
  };
  return (
    <SafeAreaView>
      <Box height={28} width={28} position="absolute"></Box>
      <Box height={62} justifyContent="center" alignItems="center"></Box>
      <Box height={44} justifyContent="center" alignItems="center">
        <Text height={44} bold textAlign="center" width={185} type="headline">
          Гар утасны дугаараа оруулна уу
        </Text>
      </Box>
      <Spacing mt={12}>
        <Box height={56} justifyContent="center" alignItems="center">
          {type == 1 && (
            <Input
              placeholder={'Утасны дугаар'}
              type={'default'}
              onChangeText={(text: string) =>
                setcred({ ...cred, number: text })
              }
            />
          )}
          {type == 2 && <DigitInput setCode={setcode} code={code} />}
          {type == 3 && (
            <Box height={128} alignItems="center" justifyContent="center">
              <Input
                placeholder={'Овог Нэр'}
                type={'default'}
                onChangeText={(text: string) =>
                  setcred({ ...cred, firsname: text })
                }
              />
              <Input
                placeholder={'Нэр'}
                type={'default'}
                onChangeText={(text: string) =>
                  setcred({ ...cred, lastname: text })
                }
              />
            </Box>
          )}
        </Box>
      </Spacing>
      <Spacing mt={35}>
        <Box height={130} justifyContent="center" alignItems="center">
          <Box width={343}>
            <Button onPress={() => Stepstep()}>
              <Text
                fontFamily={'Montserrat'}
                role={'white'}
                type={'callout'}
                textAlign={'center'}
                bold
              >
                Товчлуур
              </Text>
            </Button>
          </Box>
        </Box>
      </Spacing>
    </SafeAreaView>
  );
};
