import React, { useContext, useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { Box, Button, Spacing, Text } from '../components';
import { Input } from '../components/input';
import { AuthContext } from '../provider/auth';
import { DigitInput } from '../components/6-digit';
const { width } = Dimensions.get('screen');

export const Login = ({ navigation }: any) => {
  const { sendCode, verifyCode } = useContext(AuthContext);
  const [time, setTime] = useState(30);
  const [code, setCode] = useState('195046');
  const [type, settype] = useState(1);
  const [number, setnumber] = useState();
  useEffect(() => {
    let interval = setInterval(() => {
      setTime((time) => time - 1);
    }, 1000);
    return () => clearInterval(interval);
  }, []);
  const stepstep = () => {
    if (type == 1) {
      settype(type + 1);
      sendCode(number);
    } else {
      verifyCode(
        `${code.pin1}${code.pin2}${code.pin3}${code.pin4}${code.pin5}${code.pin6}`
      );
      settype(type + 1);
    }
  };
  return (
    <Box flexDirection="column" alignItems="center">
      <Spacing mt={5} m={3}>
        <Box
          height={20}
          flexDirection="row"
          alignItems="center"
          justifyContent="space-between"
          width={width - 60}
        ></Box>
        <Spacing mt={5}>
          <Box
            height={44}
            width="100%"
            flexDirection="row"
            justifyContent="center"
            alignItems="center"
          >
            <Text
              width={252}
              textAlign="center"
              bold
              type="headline"
              role="primary500"
              numberOfLines={2}
            >
              Таны дугаарт илгээсэн 6 оронтой кодыг оруулна уу
            </Text>
          </Box>
        </Spacing>
        <Spacing mt={12}>
          <Box justifyContent="center" alignItems="center" height={56}>
            {type == 1 ? (
              <Input
                placeholder={'Утасны дугаар'}
                type={'default'}
                onChangeText={(text: string) => setnumber(text)}
              />
            ) : (
              <DigitInput setCode={setCode} code={code} />
            )}
          </Box>
        </Spacing>
        <Spacing mt={29}>
          <Box height={20} width="100%" alignItems="center">
            <TouchableWithoutFeedback
              onPress={() => {
                time < 0 ? setTime(30) : console.log();
              }}
            >
              {/* <Text width='100%' textAlign='center' type='subheading' role={time > 0 ? 'gray' : 'primary500'}>Дахин код авах {time >= 0 ? time : ''}</Text> */}
            </TouchableWithoutFeedback>
          </Box>
        </Spacing>
        <Spacing mt={5}>
          <Button
            width="100%"
            type="primary"
            size="l"
            category="fill"
            status={type == 1 ? 'default' : code.pin1 ? 'default' : 'disabled'}
            onPress={() => {
              stepstep();
            }}
          >
            <Text role="white" bold type="callout">
              {type == 1 ? 'Дараах' : 'Нэвтрэх'}
            </Text>
          </Button>
        </Spacing>
      </Spacing>
    </Box>
  );
};
