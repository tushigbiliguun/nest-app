import React, { useState } from 'react';
import { ScrollView } from 'react-native';
import {
  Border,
  Button,
  Shadow,
  Stack,
  Queue,
  Box,
  Text,
  Spacing,
  AnimatedDynamicView,
  Avatar,
  LessonDashboard,
  BackgroundImage,
  Tag,
  Lozenge,
  Input,
  InputWithHelperTextAndCounter,
  InputWithMessage,
  InputAllInOne,
  EyeIcon,
  KeyIcon,
  LoadingCircle,
  LozengeStatus,
  Banner,
  BookIcon,
} from '../../components';
import { ClassToggle } from '../../navigation/class-toggle';
import { MainTopNav } from '../../navigation/top-nav';

const getImage = require('../../assets/images/volu.png');

export const TestScreen = () => {
  return <MainTopNav />;
};

// test Screens
export const FirstScreen = () => {
  return (
    <Spacing p={5}>
      <ScrollView>
        <Stack size={5}>
          <Box flex={1} justifyContent="center">
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Typography
              </Text>
              <Text type={'title1'}>Нэг түмэн инженер</Text>
              <Text type={'title2'}>Нэг түмэн инженер</Text>
              <Text type={'title3'}>Нэг түмэн инженер</Text>
              <Text type={'headline'}>Нэг түмэн инженер</Text>
              <Text type={'body'}>Нэг түмэн инженер</Text>
              <Text type={'callout'}>Нэг түмэн инженер</Text>
              <Text type={'subheading'}>Нэг түмэн инженер</Text>
              <Text type={'caption1'}>Нэг түмэн инженер</Text>
              <Text type={'caption2'}>Нэг түмэн инженер</Text>
              <Text numberOfLines={4} type={'body'}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                eros dui, pellentesque eget condimentum sed, interdum non
                lectus. Proin elementum risus elit, eget tempor lorem viverra
                vitae
              </Text>
            </Stack>
          </Box>
          <Box>
            <Stack width={'100%'} size={4}>
              <Text type={'title1'} bold underline>
                ClassToggle
              </Text>
              <ClassToggle />
            </Stack>
          </Box>
          <Box flex={1}>
            <Stack width={'100%'} size={4}>
              <Text type={'title1'} bold underline>
                Buttons
              </Text>
              <Queue size={5}>
                <Button onPress={() => console.log('handled')} size="s">
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Товчлуур
                  </Text>
                </Button>
                <Button onPress={() => console.log('s')}>
                  <Box flexDirection={'row'} alignItems={'center'}>
                    <Spacing mr={1.5}>
                      <Text
                        fontFamily={'Montserrat'}
                        role={'white'}
                        type={'callout'}
                        textAlign={'center'}
                        bold
                      >
                        Том товчлуур
                      </Text>
                    </Spacing>
                    <BookIcon role={'white'} height={16} width={16} />
                  </Box>
                </Button>
              </Queue>
              <Queue size={5}>
                <Button
                  onPress={() => console.log('handled')}
                  status="disabled"
                  size="s"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Товчлуур
                  </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  status="disabled"
                  size="l"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Том товчлуур
                  </Text>
                </Button>
              </Queue>
              <Queue size={5}>
                <Button
                  onPress={() => console.log('handled')}
                  status="active"
                  type="destructive"
                  size="s"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Товчлуур
                  </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  status="active"
                  type="destructive"
                  size="l"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Том товчлуур
                  </Text>
                </Button>
              </Queue>
              <Queue size={5}>
                <Button
                  onPress={() => console.log('handled')}
                  category="ghost"
                  type="destructive"
                  size="s"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'destructive500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Товчлуур
                  </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  category="ghost"
                  type="destructive"
                  size="l"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'destructive500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                  >
                    Том товчлуур
                  </Text>
                </Button>
              </Queue>
              <Queue size={5}>
                <Button
                  onPress={() => console.log('handled')}
                  category="text"
                  size="s"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'primary500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                    underline
                  >
                    Товчлуур
                  </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  category="text"
                  size="l"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'primary500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                    underline
                  >
                    Том товчлуур
                  </Text>
                </Button>
              </Queue>
              <Queue size={5}>
                <Button
                  onPress={() => console.log('handled')}
                  category="text"
                  type="destructive"
                  status="active"
                  size="s"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'destructive500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                    underline
                  >
                    Товчлуур
                  </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  category="text"
                  type="destructive"
                  status="active"
                  size="l"
                >
                  <Text
                    fontFamily={'Montserrat'}
                    role={'destructive500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold
                    underline
                  >
                    Том товчлуур
                  </Text>
                </Button>
              </Queue>
            </Stack>
          </Box>
          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Spinners
              </Text>
              <Box
                flex={1}
                flexDirection={'row'}
                justifyContent={'space-evenly'}
                flexWrap={'wrap'}
              >
                <LoadingCircle width={32} height={32} />
                <LoadingCircle width={32} height={32} />
              </Box>
            </Stack>
          </Box>
        </Stack>
      </ScrollView>
    </Spacing>
  );
};

export const SecondScreen = () => {
  return (
    <Spacing p={5}>
      <ScrollView>
        <Stack size={5}>
          <Box flex={1} justifyContent="center">
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Inputs
              </Text>
              <Input
                placeholder={'Овог Нэр'}
                type={'default'}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <Input
                placeholder={'Овог Нэр'}
                type={'default'}
                LeftIcon={EyeIcon}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <Input
                placeholder={'Овог Нэр'}
                type={'default'}
                RigthIcon={KeyIcon}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <Input
                placeholder={'Овог Нэр'}
                type={'password'}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <InputWithHelperTextAndCounter
                placeholder={'Яаралтай үед холбоо барих'}
                counter={true}
                helperText={'Туслах текст '}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <InputWithMessage
                placeholder={'Овог Нэр nani'}
                messageText={'This is message'}
                messageType={'default'}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
              <InputAllInOne
                placeholder={'Овог Нэр nani'}
                messageText={'This is message'}
                messageType={'success'}
                counter={true}
                helperText={'Туслах текст '}
                onChangeText={(text: string) => console.log(text)}
                onSubmitEditing={() => console.log('nani')}
              />
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Tags
              </Text>
              <Stack size={4}>
                <Queue size={2}>
                  <Tag category={'default'} type={'unremoveable'}>
                    Tag
                  </Tag>
                  <Tag category={'default'} type={'removeable'}>
                    Tag
                  </Tag>
                </Queue>
                <Queue size={2}>
                  <Tag category={'message'} type={'success'}>
                    Success
                  </Tag>
                  <Tag category={'message'} type={'danger'}>
                    Danger
                  </Tag>
                  <Tag category={'message'} type={'warning'}>
                    Warning
                  </Tag>
                </Queue>
                <Queue size={2}>
                  <Tag category={'rounded'} type={'user'}>
                    Бямбадорж
                  </Tag>
                  <Tag
                    category={'rounded'}
                    type={'user'}
                    avatarUrl={
                      'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                    }
                  >
                    Бямбадорж
                  </Tag>
                </Queue>
              </Stack>
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Lesson Dashboard
              </Text>
              <Box flex={1} flexDirection={'column'}>
                <LessonDashboard
                  url={
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqwnCanQ7dgSfYKymyi1b0rx23l1YeoDacjw&usqp=CAU'
                  }
                  type={'locked'}
                  name={'Userflow'}
                  desc={'Хичээл 2'}
                  onPress={() => console.log('pressed')}
                />
              </Box>
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={5}>
              <Text type={'title1'} bold underline>
                Banners
              </Text>
              <Banner type={'warning'} title={'Мэдээ'}>
                <Text numberOfLines={3} type={'callout'}>
                  Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!
                </Text>
                <Button category={'text'} type={'primary'} onPress={() => {}}>
                  <Text
                    type={'footnote'}
                    fontFamily={'Montserrat'}
                    underline
                    bold
                  >
                    Мэдээлэл авах
                  </Text>
                </Button>
              </Banner>
              <Banner type={'critical'} title={'Анхааруулга'}>
                <Text numberOfLines={3} type={'callout'}>
                  Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!
                </Text>
                <Button category={'text'} type={'primary'} onPress={() => {}}>
                  <Text
                    type={'footnote'}
                    fontFamily={'Montserrat'}
                    underline
                    bold
                  >
                    Мэдээлэл авах
                  </Text>
                </Button>
              </Banner>
              <Banner type={'success'} title={'Анхааруулга'}>
                <Text numberOfLines={3} type={'callout'}>
                  Шинэ элсэлт дуусахад 23 цаг үлдсэн байна. Та яараарай!
                </Text>
              </Banner>
            </Stack>
          </Box>
        </Stack>
      </ScrollView>
    </Spacing>
  );
};

export const ThirdScreen = () => {
  const [show, setShow] = useState(true);
  return (
    <Spacing p={5}>
      <ScrollView>
        <Stack size={5}>
          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Lozenge
              </Text>
              <Queue size={4}>
                <Lozenge type={'success'} style={'subtle'}>
                  АМЖИЛТТАЙ
                </Lozenge>
                <Lozenge type={'default'} style={'subtle'}>
                  DEFAULT
                </Lozenge>
              </Queue>
              <Queue size={4}>
                <Lozenge type={'pending'} style={'subtle'}>
                  ХҮЛЭЭГДЭЖ БАЙНА
                </Lozenge>
                <Lozenge type={'error'} style={'subtle'}>
                  АМЖИЛТГҮЙ
                </Lozenge>
              </Queue>
              <Queue size={4}>
                <Lozenge type={'success'} style={'bold'}>
                  АМЖИЛТТАЙ
                </Lozenge>
                <Lozenge type={'default'} style={'bold'}>
                  DEFAULT
                </Lozenge>
              </Queue>
              <Queue size={4}>
                <Lozenge type={'pending'} style={'bold'}>
                  ХҮЛЭЭГДЭЖ БАЙНА
                </Lozenge>
                <Lozenge type={'error'} style={'bold'}>
                  АМЖИЛТГҮЙ
                </Lozenge>
              </Queue>
              <Text type={'title1'} bold underline>
                Lozenge Status
              </Text>
              <LozengeStatus
                type={'default'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'success'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'pending'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'error'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'default'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'success'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'pending'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'error'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
                id={'HOPF21-0251'}
              />
              <LozengeStatus
                type={'default'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'success'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'pending'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'error'}
                style={'subtle'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'default'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'success'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'pending'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
              <LozengeStatus
                type={'error'}
                style={'bold'}
                status={'ХҮЛЭЭГДЭЖ БАЙНА'}
              />
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title3'} bold underline>
                Dynamic animated view
              </Text>
              <Spacing ph={2}>
                <AnimatedDynamicView
                  visible={show}
                  duration={500}
                  width={'100%'}
                  height={1000}
                >
                  <Shadow
                    h={2}
                    w={0}
                    radius={4}
                    opacity={0.25}
                    role={'primary500'}
                  >
                    <Shadow
                      radius={2}
                      h={0}
                      w={0}
                      opacity={0.25}
                      role={'primary500'}
                    >
                      <Border radius={20}>
                        <Box width={'100%'} height={'auto'} role={'white'}>
                          <Spacing p={5}>
                            <Text bold type={'headline'}>
                              Dynamic animated view
                            </Text>
                            <Text type={'body'}>
                              Lorem Ipsum is simply dummy text of the printing
                              and typesetting industry. Lorem Ipsum has been the
                              industry's standard dummy text ever since the
                              1500s, when an unknown printer took a galley of
                              type and scrambled it to make a type specimen
                              book. It has survived not only five centuries, but
                              also the leap into electronic typesetting,
                              remaining essentially unchanged. It was
                              popularised in the 1960s with the release of
                              Letraset sheets containing Lorem Ipsum passages,
                              and more recently with desktop publishing software
                              like Aldus PageMaker including versions of Lorem
                              Ipsum
                            </Text>
                          </Spacing>
                        </Box>
                      </Border>
                    </Shadow>
                  </Shadow>
                </AnimatedDynamicView>
              </Spacing>
              <Button
                onPress={() => setShow((show) => !show)}
                size="s"
                width={200}
              >
                <Text
                  fontFamily={'Montserrat'}
                  role={'white'}
                  type={'callout'}
                  textAlign={'center'}
                  bold
                >
                  Солих
                </Text>
              </Button>
              <Text type={'title1'} bold underline>
                Background Image
              </Text>
              <Box>
                <BackgroundImage
                  source={getImage}
                  height={164}
                  width={167}
                ></BackgroundImage>
              </Box>
            </Stack>
          </Box>

          <Box flex={1}>
            <Stack size={2}>
              <Text type={'title1'} bold underline>
                Avatars
              </Text>
              <Box
                flex={1}
                flexDirection={'row'}
                justifyContent={'space-evenly'}
                flexWrap={'wrap'}
              >
                <Avatar
                  size={'L'}
                  url={
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                  }
                />
                <Avatar
                  size={'M'}
                  url={
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                  }
                />
                <Avatar
                  size={'S'}
                  url={
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                  }
                />
                <Avatar
                  size={'XS'}
                  url={
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Alesso_profile.png/467px-Alesso_profile.png'
                  }
                />
                <Avatar size={'L'} />
                <Avatar size={'M'} />
                <Avatar size={'S'} />
              </Box>
            </Stack>
          </Box>
        </Stack>
      </ScrollView>
    </Spacing>
  );
};
