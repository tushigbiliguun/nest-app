import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Text, Button, Spacing, Box } from '../../components';
import { useNavigation } from '@react-navigation/native';
import { NavigationRoutes } from '../../navigation/navigation-params';

export const SplashSecond = () => {
  const navigation = useNavigation();

  return (
    <Box flex={1} justifyContent="center" alignItems="center">
      <Box flex={1} position={'absolute'} top={0} right={10} zIndex={5}>
        <Button
          category={'text'}
          size={'s'}
          status={'active'}
          onPress={() => navigation.navigate(NavigationRoutes.MainRoot)}
          width={95}
        >
          <Text fontFamily={'Montserrat'} bold type={'body'}>
            АЛГАСАХ
          </Text>
        </Button>
      </Box>

      <Box flex={1} width={'100%'}>
        <Spacing ph={15} pv={10} grow={1}>
          <Box flex={1} justifyContent='center' alignItems='center'>
            <Box flex={1} justifyContent='center' alignItems='center'>
              <Image source={require('../../assets/images/splash-second.png')} style={{ marginBottom: 50 }} />
              <Spacing mt={8}>
                <Text type={'title2'} bold textAlign={'center'}>Шинэ мэдлэг</Text>
              </Spacing>
              <Spacing mt={6} mb={12}>
                <Text type={'body'} textAlign={'center'} role={'primary400'} width={250}>Бидний үнэгүй мини-сургалтуудыг сонирхон өөрийн мэдлэгээ нэмэгдүүлээрэй</Text>
              </Spacing>
            </Box>
            <Box height={20} flexDirection='row' justifyContent='center' alignItems='center'>
              <View style={[styles.indicator, styles.unSelected]} />
              <View style={[styles.indicator, styles.selected]} />
              <View style={[styles.indicator, styles.unSelected]} />
            </Box>
          </Box>
        </Spacing>
      </Box>
    </Box>
  );
};

const styles = StyleSheet.create({
  indicator: {
    width: 9,
    height: 9,
    borderRadius: 5,
    marginHorizontal: 5,
  },
  selected: {
    backgroundColor: '#52575C',
  },
  unSelected: {
    backgroundColor: '#E8E8E8',
  },
});
