export enum NavigationRoutes {
  MainRoot = 'MainRoot',
  Home = 'Home',
  Course = 'Course',
  Profile = 'Profile',
  Test = 'Test',
  Splash = 'Splash',
  ExamWarning1 = 'ExamWarning1',
  ExamWarning2 = 'ExamWarning2',
  ExamWarning3 = 'ExamWarning3',
  ExamWarning4 = 'ExamWarning4',
}
export interface NavigationPayload<T> {
  props: T;
}
export type NavigatorParamList = {
  [NavigationRoutes.Splash]: NavigationPayload<any>;
  [NavigationRoutes.MainRoot]: NavigationPayload<any>;
  [NavigationRoutes.Home]: NavigationPayload<any>;
  [NavigationRoutes.Course]: NavigationPayload<any>;
  [NavigationRoutes.Profile]: NavigationPayload<any>;
  [NavigationRoutes.Test]: NavigationPayload<any>;
  [NavigationRoutes.Splash]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning1]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning2]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning3]: NavigationPayload<any>;
  [NavigationRoutes.ExamWarning4]: NavigationPayload<any>;
};
