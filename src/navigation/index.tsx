import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import {
  TestScreen,
  ExamWarningScreen1,
  ExamWarningScreen2,
  ExamWarningScreen3,
  ExamWarningScreen4,
} from '../screen';
import MainBottomNavigation from './main-bottom-navigation';
import { SplashScreen } from '../screen';
import { NavigationRoutes, NavigatorParamList } from './navigation-params';

const RootStack = createStackNavigator<NavigatorParamList>();

export const RootNavigationContainer = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator initialRouteName={NavigationRoutes.Splash}>
        {/* <RootStack.Screen
                    name={NavigationRoutes.Splash}
                    component={SplashScreen}
                    options={{ headerShown: false }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.ExamWarning1}
                    component={ExamWarningScreen1}
                    options={{ headerShown: false, gestureEnabled: true }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.ExamWarning2}
                    component={ExamWarningScreen2}
                    options={{ headerShown: false, gestureEnabled: true }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.ExamWarning3}
                    component={ExamWarningScreen3}
                    options={{ headerShown: false, gestureEnabled: true }}
                />
                <RootStack.Screen
                    name={NavigationRoutes.ExamWarning4}
                    component={ExamWarningScreen4}
                    options={{ headerShown: false, gestureEnabled: true }}
                /> */}
        <RootStack.Screen
          name={NavigationRoutes.MainRoot}
          component={MainBottomNavigation}
          options={{ headerShown: false }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};
