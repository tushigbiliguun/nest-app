import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

interface Props {
  width?: number | string;
  height?: number | string;
  color?: string;
}

export const ArrowIcon: React.FC<Props> = ({
  width = 14,
  height = 7,
  color = '#303030',
  ...others
}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 14 7"
      fill="none"
      {...others}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7 7a.993.993 0 01-.64-.232l-6-5A1 1 0 111.64.232l5.37 4.476L12.374.393a1 1 0 011.254 1.558l-6 4.828A1 1 0 017 7z"
        fill={color}
      />
    </Svg>
  );
};
