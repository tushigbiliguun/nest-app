import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { useTheme } from '../theme-provider';
import { IconType } from '../types';

export const CheckIcon: React.FC<IconType> = ({
  height = 15,
  width = 15,
  role = 'success500',
}) => {
  const { colors } = useTheme();
  return (
    <Svg width={width} height={height} viewBox="0 0 16 15" fill="none">
      <Path
        d="M8 15C3.85775 15 0.5 11.6423 0.5 7.5C0.5 3.35775 3.85775 0 8 0C12.1423 0 15.5 3.35775 15.5 7.5C15.5 11.6423 12.1423 15 8 15ZM8 13.5C9.5913 13.5 11.1174 12.8679 12.2426 11.7426C13.3679 10.6174 14 9.0913 14 7.5C14 5.9087 13.3679 4.38258 12.2426 3.25736C11.1174 2.13214 9.5913 1.5 8 1.5C6.4087 1.5 4.88258 2.13214 3.75736 3.25736C2.63214 4.38258 2 5.9087 2 7.5C2 9.0913 2.63214 10.6174 3.75736 11.7426C4.88258 12.8679 6.4087 13.5 8 13.5ZM7.25225 10.5L4.07 7.31775L5.1305 6.25725L7.25225 8.379L11.4943 4.13625L12.5555 5.19675L7.25225 10.5Z"
        fill={colors[role]}
      />
    </Svg>
  );
};
