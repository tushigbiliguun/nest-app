import React from 'react';
import { View, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import { Text } from './core';
import { Box, Spacing } from './layout';

export const Dropdown = () => {
  return (
    <TouchableOpacity>
      <Spacing pl={3} pr={3}>
        <Box
          height={56}
          role={'primary100'}
          bColor={'#172B4D'}
          bWidth={2}
          bRadius={4}
        >
          <Spacing pl={4} pr={4} pt={2} pb={2}>
            <Box
              flexDirection={'row'}
              width={'100%'}
              height={'100%'}
              alignItems={'center'}
              justifyContent={'center'}
            >
              <Box flexDirection="column">
                <Box flexDirection={'row'} height={20} width={90}>
                  <Text role={'primary400'} type={'caption1'}>
                    Сонголт
                  </Text>
                  <Text role={'destructive500'} type={'caption1'}>
                    *
                  </Text>
                </Box>
                <Text role={'primary400'} type={'body'}>
                  Сонгох...
                </Text>
              </Box>
              <Box width={10} height={10} role={'primary400'}></Box>
            </Box>
          </Spacing>
        </Box>
      </Spacing>
    </TouchableOpacity>
  );
};
