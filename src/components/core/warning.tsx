import React from 'react';
import {
  Box,
  Text,
  Shadow,
  Button,
  Spacing,
  RightArrowIcon,
} from '../../components';
import { Border } from './border';

export const Warning: React.FC<WarningType> = ({
  title,
  buttonname,
  children,
  onPress,
  titleAlign = 'center',
}) => {
  return (
    <Shadow role="primary500" opacity={0.25} w={2}>
      <Shadow role="primary500" opacity={0.15} w={2}>
        <Border radius={4}>
          <Box height={212} width={343} role="primary100">
            <Box flex={1} role="primary100" alignItems="center">
              <Spacing mt={6}>
                {titleAlign === 'center' ? (
                  <Text
                    role="primary500"
                    type="title3"
                    bold={true}
                    textAlign="center"
                  >
                    {title}
                  </Text>
                ) : (
                  <Box width={280} height={30} justifyContent="flex-start">
                    <Text
                      role="primary500"
                      type="title3"
                      textAlign="left"
                      bold={true}
                    >
                      {title}
                    </Text>
                  </Box>
                )}
              </Spacing>
              <Spacing mt={4}>{children}</Spacing>
            </Box>
            <Button onPress={onPress} category="fill" size="l">
              <Box
                flexDirection="row"
                width={90}
                justifyContent="center"
                alignItems="center"
              >
                <Text
                  type="callout"
                  role="white"
                  bold={true}
                  fontFamily="Montserrat"
                >
                  {buttonname.toUpperCase()}
                </Text>
                <RightArrowIcon />
              </Box>
            </Button>
          </Box>
        </Border>
      </Shadow>
    </Shadow>
  );
};

type WarningType = {
  title: string;
  buttonname: string;
  onPress: Function;
  children?: any;
  titleAlign?: 'center' | 'left';
};
